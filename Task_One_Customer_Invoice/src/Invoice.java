import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Invoice{
    public int uniqueId;
    public int customerId;
    public Customer customer;
    List<Product> productList;

    public Invoice(int uniqueId, int customerId, Customer customer, List<Product> productList) {
        this.uniqueId = uniqueId;
        this.customerId = customerId;
        this.customer = customer;
        this.productList = productList;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
