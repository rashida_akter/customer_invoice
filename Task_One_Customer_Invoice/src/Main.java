import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        Product[] productObj = new Product[5];
        Customer[] customerObj = new Customer[5];
        Invoice[] invoiceObj = new Invoice[5];
        ArrayList<Integer> orders = new ArrayList<Integer>();
        ArrayList<Product> products = new ArrayList<Product>();

        productObj[0] = new Product(1, "BournVille");
        productObj[1] = new Product(2, "Amul");
        productObj[2] = new Product(3, "Fruits & Nuts");
        productObj[3] = new Product(4, "Bountry");
        productObj[4] = new Product(5, "Bubbly");

        customerObj[0] = new Customer(1, "Mr. riasat");
        customerObj[1] = new Customer(2, "Mr. Abraham");
        customerObj[2] = new Customer(3, "Mr.Devid");
        customerObj[3] = new Customer(4, "Mr. John");
        customerObj[4] = new Customer(5, "Mr. whale");

        products.add(productObj[0]);
        products.add(productObj[1]);
        products.add(productObj[2]);
        products.add(productObj[3]);
        products.add(productObj[4]);


        invoiceObj[0] = new Invoice(1, 1, customerObj[0], products);
        invoiceObj[1] = new Invoice(2, 2,customerObj[1], products);
        invoiceObj[2] = new Invoice(3, 3, customerObj[2], products);
        invoiceObj[3] = new Invoice(4, 4, customerObj[3], products);
        invoiceObj[4] = new Invoice(5, 5, customerObj[4], products);

        System.out.println(invoiceObj[0].customer.getCustomerName());
        
        for(Product pObj : invoiceObj[4].productList){
            System.out.println(pObj.getProductName());
        }

    }
}
